SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `bird`;
DROP TABLE IF EXISTS `bird_location`;
DROP TABLE IF EXISTS `location`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `colors`;


SET FOREIGN_KEY_CHECKS=1;

CREATE TABLE `user` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(190) NOT NULL,
    `password` VARCHAR(190) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE(username)
);
INSERT INTO `user` (username, password) VALUES ('admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');

CREATE TABLE IF NOT EXISTS `bird` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `soundURL` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `flightMode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flightPattern` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basicColor` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `headColor` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `headColorCheek` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `headColorForehead` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `headColorBack` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `headColorNeck` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wingsColor` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wingsColorAbove` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wingsColorBelow` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wingsColorPattern` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tailColor` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tailColorAbove` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tailColorBelow` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tailColorPattern` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chestColor` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `chestColorPattern` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `bird_location` (
  `bird_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  PRIMARY KEY (`bird_id`,`location_id`),
  KEY `FK_bird_location_location` (`location_id`,`bird_id`),
  CONSTRAINT `FK_bird_location_birdsfilters` FOREIGN KEY (`bird_id`) REFERENCES `bird` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_bird_location_location` FOREIGN KEY (`location_id`) REFERENCES `location` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


CREATE TABLE IF NOT EXISTS `colors` (
  `key` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;







