package birds.api.model;

import java.sql.Array;
import java.util.List;

public class Bird {
    private int id;
    private String photo;
    private String soundURL;
    private String name;
    private String flightMode;
    private String flightPattern;
    private String basicColor;
    private String headColor;
    private String headColorCheek;
    private String headColorForehead;
    private String headColorBack;
    private String headColorNeck;
    private String wingsColor;
    private String wingsColorAbove;
    private String wingsColorBelow;
    private String wingsColorPattern;
    private String tailColor;
    private String tailColorAbove;
    private String tailColorBelow;
    private String tailColorPattern;
    private String chestColor;
    private String chestColorPattern;
    private List<String> locations;
    private String desc;

    public Bird(int id, String photo, String soundURL, String name, String flightMode, String flightPattern, String basicColor, String headColor, String headColorCheek, String headColorForehead, String headColorBack, String headColorNeck, String wingsColor, String wingsColorAbove, String wingsColorBelow, String wingsColorPattern, String tailColor, String tailColorAbove, String tailColorBelow, String tailColorPattern, String chestColor, String chestColorPattern, List<String> locations) {
        this.id = id;
        this.photo = photo;
        this.soundURL = soundURL;
        this.name = name;
        this.flightMode = flightMode;
        this.flightPattern = flightPattern;
        this.basicColor = basicColor;
        this.headColor = headColor;
        this.headColorCheek = headColorCheek;
        this.headColorForehead = headColorForehead;
        this.headColorBack = headColorBack;
        this.headColorNeck = headColorNeck;
        this.wingsColor = wingsColor;
        this.wingsColorAbove = wingsColorAbove;
        this.wingsColorBelow = wingsColorBelow;
        this.wingsColorPattern = wingsColorPattern;
        this.tailColor = tailColor;
        this.tailColorAbove = tailColorAbove;
        this.tailColorBelow = tailColorBelow;
        this.tailColorPattern = tailColorPattern;
        this.chestColor = chestColor;
        this.chestColorPattern = chestColorPattern;
        this.locations = locations;
    }

    public String getSoundURL() { return soundURL; }

    public void setSoundURL(String soundURL) { this.soundURL = soundURL; }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<String> getLocations() {
        return locations;
    }

    public void setLocations(List<String> locations) {
        this.locations = locations;
    }

    public int getId() {
        return id;
    }
    public String getPhoto() {
        return photo;
    }

    public String getName() {
        return name;
    }

    public String getFlightMode() {
        return flightMode;
    }

    public String getFlightPattern() {
        return flightPattern;
    }

    public String getBasicColor() {
        return basicColor;
    }

    public String getHeadColor() {
        return headColor;
    }

    public String getHeadColorCheek() {
        return headColorCheek;
    }

    public String getHeadColorForehead() {
        return headColorForehead;
    }

    public String getHeadColorBack() {
        return headColorBack;
    }

    public String getHeadColorNeck() {
        return headColorNeck;
    }

    public String getWingsColor() {
        return wingsColor;
    }

    public String getWingsColorAbove() {
        return wingsColorAbove;
    }

    public String getWingsColorBelow() {
        return wingsColorBelow;
    }

    public String getWingsColorPattern() {
        return wingsColorPattern;
    }

    public String getTailColor() {
        return tailColor;
    }

    public String getTailColorAbove() {
        return tailColorAbove;
    }

    public String getTailColorBelow() {
        return tailColorBelow;
    }

    public String getTailColorPattern() {
        return tailColorPattern;
    }

    public String getChestColor() {
        return chestColor;
    }

    public String getChestColorPattern() {
        return chestColorPattern;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFlightMode(String flightMode) {
        this.flightMode = flightMode;
    }

    public void setFlightPattern(String flightPattern) {
        this.flightPattern = flightPattern;
    }

    public void setBasicColor(String basicColor) {
        this.basicColor = basicColor;
    }

    public void setHeadColor(String headColor) {
        this.headColor = headColor;
    }

    public void setHeadColorCheek(String headColorCheek) {
        this.headColorCheek = headColorCheek;
    }

    public void setHeadColorForehead(String headColorForehead) {
        this.headColorForehead = headColorForehead;
    }

    public void setHeadColorBack(String headColorBack) {
        this.headColorBack = headColorBack;
    }

    public void setHeadColorNeck(String headColorNeck) {
        this.headColorNeck = headColorNeck;
    }

    public void setWingsColor(String wingsColor) {
        this.wingsColor = wingsColor;
    }

    public void setWingsColorAbove(String wingsColorAbove) {
        this.wingsColorAbove = wingsColorAbove;
    }

    public void setWingsColorBelow(String wingsColorBelow) {
        this.wingsColorBelow = wingsColorBelow;
    }

    public void setWingsColorPattern(String wingsColorPattern) {
        this.wingsColorPattern = wingsColorPattern;
    }

    public void setTailColor(String tailColor) {
        this.tailColor = tailColor;
    }

    public void setTailColorAbove(String tailColorAbove) {
        this.tailColorAbove = tailColorAbove;
    }

    public void setTailColorBelow(String tailColorBelow) {
        this.tailColorBelow = tailColorBelow;
    }

    public void setTailColorPattern(String tailColorPattern) {
        this.tailColorPattern = tailColorPattern;
    }

    public void setChestColor(String chestColor) {
        this.chestColor = chestColor;
    }

    public void setChestColorPattern(String chestColorPattern) {
        this.chestColorPattern = chestColorPattern;
    }
}
