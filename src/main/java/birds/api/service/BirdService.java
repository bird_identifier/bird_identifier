package birds.api.service;

import birds.api.model.Bird;
import birds.api.repository.BirdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class BirdService {

    private Map<String, String> wikiDescs = new HashMap<>();

    @Autowired
    private BirdRepository birdRepository;

    @Autowired
    private WikiApiService wikiApiService;

    public List<Bird> getBirds() {
        List<Bird> birds = birdRepository.getBirds();

        for (Bird bird : birds) {
            if (wikiDescs.containsKey(bird.getName())) {
                bird.setDesc(wikiDescs.get(bird.getName()));
            } else {
                String birdName = bird.getName();
                String birdDescription = wikiApiService.getPostsPlainJSON(birdName);
                wikiDescs.put(birdName, birdDescription);
                bird.setDesc(wikiDescs.get(bird.getName())); //Korda seda kuidagi kui pea terav on..
            }
//            String birdName = bird.getName();
//            String birdDescription = wikiApiService.getPostsPlainJSON(birdName);
//            bird.setDesc(birdDescription);
        }

        return birds;
    }

}
