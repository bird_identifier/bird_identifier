package birds.api.service;

//import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Queue;

@Service
public class WikiApiService {
//    private final RestTemplate restTemplate
    private RestTemplate restTemplate = new RestTemplate();

//    public WikiApiService(RestTemplateBuilder restTemplateBuilder) {
//        this.restTemplate = restTemplateBuilder.build();
//    }

    public String getPostsPlainJSON(String name) {
        try {
            String url = "https://en.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro&explaintext&redirects=1&titles=" + name;
            JsonResponse response = this.restTemplate.getForObject(url, JsonResponse.class);

            Object pageID = ((LinkedHashMap) response.getQuery().getPages()).keySet().toArray()[0];
            String description = ((LinkedHashMap<String, String>) response.getQuery().getPages().get(pageID)).get("extract");

            return description;
        } catch (Exception e) {
            return "Wikipedia api service down";
        }
    }


    public static class JsonResponse {

        private Query query;

        public JsonResponse() {
        }

        public Query getQuery() {
            return query;
        }

        public void setQuery(Query query) {
            this.query = query;
        }
    }

    public static class Query {

        private Map<String, Object> pages;

        public Query() {
        }

        public Map<String, Object> getPages() {
            return pages;
        }

        public void setPages(Map<String, Object> pages) {
            this.pages = pages;
        }
    }

//    public static class Pages {
//        private PageId pageId;
//
//        public Pages() {
//
//        }
//
//        public PageId getPageId() {
//            return pageId;
//        }
//
//        public void setPageId(PageId pageId) {
//            this.pageId = pageId;
//        }
//    }
//
//    public static class PageId {
//        private String extract;
//
//        public String getExtract() {
//            return extract;
//        }
//
//        public void setExtract(String extract) {
//            this.extract = extract;
//        }
//    }
}
