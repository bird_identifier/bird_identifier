package birds.api.repository;

import birds.api.model.Bird;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class BirdRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private List<String> getBirdLocations(int birdId) {
        return jdbcTemplate.query("SELECT location.location FROM location\n" +
                        "INNER JOIN bird_location\n" +
                        "ON bird_location.location_id = location.id\n" +
                        "WHERE bird_location.bird_id = ?;",
                new Object[]{birdId}, (row, rowNum) -> {
                    return row.getString("location");
                });
    }

    public List<Bird> getBirds() {
        return jdbcTemplate.query("select * from bird", (row, number) -> {
            return new Bird(
                    row.getInt("id"),
                    row.getString("photo"),
                    row.getString("soundURL"),
                    row.getString("name"),
                    row.getString("flightMode"),
                    row.getString("flightPattern"),
                    row.getString("basicColor"),
                    row.getString("headColor"),
                    row.getString("headColorCheek"),
                    row.getString("headColorForehead"),
                    row.getString("headColorBack"),
                    row.getString("headColorNeck"),
                    row.getString("wingsColor"),
                    row.getString("wingsColorAbove"),
                    row.getString("wingsColorBelow"),
                    row.getString("wingsColorPattern"),
                    row.getString("tailColor"),
                    row.getString("tailColorAbove"),
                    row.getString("tailColorBelow"),
                    row.getString("tailColorPattern"),
                    row.getString("chestColor"),
                    row.getString("chestColorPattern"),
                    this.getBirdLocations(row.getInt("id"))
            );
        });
    }

    // return bird by id
    public Bird getBird(int id) {
        List<Bird> birdList = jdbcTemplate.query("select * from bird where id = ?",
                new Object[]{id},
                (row, number) -> {
                    return new Bird(
                            row.getInt("id"),
                            row.getString("photo"),
                            row.getString("soundURL"),
                            row.getString("name"),
                            row.getString("flightMode"),
                            row.getString("flightPattern"),
                            row.getString("basicColor"),
                            row.getString("headColor"),
                            row.getString("headColorCheek"),
                            row.getString("headColorForehead"),
                            row.getString("headColorBack"),
                            row.getString("headColorNeck"),
                            row.getString("wingsColor"),
                            row.getString("wingsColorAbove"),
                            row.getString("wingsColorBelow"),
                            row.getString("wingsColorPattern"),
                            row.getString("tailColor"),
                            row.getString("tailColorAbove"),
                            row.getString("tailColorBelow"),
                            row.getString("tailColorPattern"),
                            row.getString("chestColor"),
                            row.getString("chestColorPattern"),
                            this.getBirdLocations(row.getInt("id"))
                    );
                });
        if(birdList.size() > 0) {
            return birdList.get(0);
        } else {
            return null;
        }
    }

    public void addBird(Bird bird) {
//        jdbcTemplate.update("insert into bird (`name`, photo, flightMode, flightPattern, basicColor, headColor, headColorCheek, headColorForehead, headColorBack, headColorNeck, wingsColor, wingsColorAbove, wingsColorBelow, wingsColorPattern, tailColor, tailColorAbove, tailColorBelow, tailColorPattern, chestColor, chestColorPattern) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
//                bird.getName(), bird.getPhoto(), bird.getFlightMode(), bird.getFlightPattern(), bird.getBasicColor(), bird.getHeadColor(), bird.getHeadColorCheek(), bird.getHeadColorForehead(), bird.getHeadColorBack(), bird.getHeadColorNeck(), bird.getWingsColor(), bird.getWingsColorAbove(), bird.getWingsColorBelow(), bird.getWingsColorPattern(), bird.getTailColor(), bird.getTailColorAbove(), bird.getTailColorBelow(), bird.getTailColorPattern(), bird.getChestColor(), bird.getChestColorPattern()
//        );

        String sql = "insert into bird (`name`, soundURL, photo, flightMode, flightPattern, basicColor, headColor, headColorCheek, headColorForehead, headColorBack, headColorNeck, wingsColor, wingsColorAbove, wingsColorBelow, wingsColorPattern, tailColor, tailColorAbove, tailColorBelow, tailColorPattern, chestColor, chestColorPattern) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        String name = bird.getName();
        String soundURL = bird.getSoundURL();
        String photo = bird.getPhoto();
        String flightMode = bird.getFlightMode();
        String flightPattern = bird.getFlightPattern();
        String basicColor = bird.getBasicColor();
        String headColor = bird.getHeadColor();
        String headColorCheek = bird.getHeadColorCheek();
        String headColorForehead = bird.getHeadColorForehead();
        String headColorBack = bird.getHeadColorBack();
        String headColorNeck = bird.getHeadColorNeck();
        String wingsColor = bird.getWingsColor();
        String wingsColorAbove = bird.getWingsColorAbove();
        String wingsColorBelow = bird.getWingsColorBelow();
        String wingsColorPattern = bird.getWingsColorPattern();
        String tailColor = bird.getTailColor();
        String tailColorAbove = bird.getTailColorAbove();
        String tailColorBelow = bird.getTailColorBelow();
        String tailColorPattern = bird.getTailColorPattern();
        String chestColor = bird.getChestColor();
        String chestColorPattern = bird.getChestColorPattern();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
                    PreparedStatement ps =
                            connection.prepareStatement(
                                    sql,
                                    Statement.RETURN_GENERATED_KEYS
                            );
                    ps.setString(1, name);
                    ps.setString(3, soundURL);
                    ps.setString(2, photo);
                    ps.setString(4, flightMode);
                    ps.setString(5, flightPattern);
                    ps.setString(6, basicColor);
                    ps.setString(7, headColor);
                    ps.setString(8, headColorCheek);
                    ps.setString(9, headColorForehead);
                    ps.setString(10, headColorBack);
                    ps.setString(11, headColorNeck);
                    ps.setString(12, wingsColor);
                    ps.setString(13, wingsColorAbove);
                    ps.setString(14, wingsColorBelow);
                    ps.setString(15, wingsColorPattern);
                    ps.setString(16, tailColor);
                    ps.setString(17, tailColorAbove);
                    ps.setString(18, tailColorBelow);
                    ps.setString(19, tailColorPattern);
                    ps.setString(20, chestColor);
                    ps.setString(21, chestColorPattern);
                    return ps;
                },
                keyHolder
        );
        int newBirdId = keyHolder.getKey().intValue();

        // Siin kutsume uue funtksiooni mis sisestab asukohad
        addBirdLocations(newBirdId, bird);
    }

    private void addBirdLocations(int id, Bird bird) {
        List<String> locations = bird.getLocations();
        //System.out.println(locations);
        //jdbcTemplate.query("SELECT id FROM location WHERE location = ?;", );
        for (int i = 0; i < bird.getLocations().size(); i++) {
            jdbcTemplate.update("INSERT INTO `bird_location` (`bird_id`, `location_id`) VALUES\n" +
                    "\t(?, ?);", id, getLocationId(locations.get(i)));
        }
    }

    public int getLocationId(String locationName) {
        return jdbcTemplate.queryForObject("select id from location where location.location = ?", new Object[]{locationName}, Integer.class);
    }

    public void deleteBird(int id) {
        jdbcTemplate.update("delete from bird where id=?", id);
    }

    public void updateBird(Bird bird) {
        jdbcTemplate.update(

                 "update bird set `name` = ?, soundURL = ?, photo = ?, flightMode = ?, flightPattern = ?, basicColor = ?, headColor = ?, headColorCheek = ?, headColorForehead = ?, headColorBack = ?, headColorNeck = ?, wingsColor = ?, wingsColorAbove = ?, wingsColorBelow = ?, wingsColorPattern = ?, tailColor = ?, tailColorAbove = ?, tailColorBelow = ?, tailColorPattern = ?, chestColor = ?, chestColorPattern = ? where id = ?",


        bird.getName(),
        bird.getSoundURL(),
        bird.getPhoto(),
        bird.getFlightMode(),
        bird.getFlightPattern(),
        bird.getBasicColor(),
        bird.getHeadColor(),
        bird.getHeadColorCheek(),
        bird.getHeadColorForehead(),
        bird.getHeadColorBack(),
        bird.getHeadColorNeck(),
        bird.getWingsColor(),
        bird.getWingsColorAbove(),
        bird.getWingsColorBelow(),
        bird.getWingsColorPattern(),
        bird.getTailColor(),
        bird.getTailColorAbove(),
        bird.getTailColorBelow(),
        bird.getTailColorPattern(),
        bird.getChestColor(),
        bird.getChestColorPattern(),
        bird.getId()
        );
    }

}
