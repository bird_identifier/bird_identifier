package birds.api.repository;


import birds.api.model.Color;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

@Repository
public class ColorRepository {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void addColor(Color color) {
        String sql = "insert into colors (key, text) values(?, ?)";
        String key = color.getKey();
        String text = color.getText();

        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(connection -> {
            PreparedStatement ps =
                    connection.prepareStatement(
                            sql,
                            Statement.RETURN_GENERATED_KEYS
                    );
            ps.setString(1, key);
            ps.setString(2, text);
            return ps;
        }, keyHolder);
    }

    public List<Color> getColors() {
        return jdbcTemplate.query("select * from colors", (row, number) -> {
            return new Color(
                    row.getString("key"),
                    row.getString("text")

            );
        });
    }
//        public void deleteColor (String key) {
//            jdbcTemplate.update("delete from colors where key=?", key);
//        }
}
