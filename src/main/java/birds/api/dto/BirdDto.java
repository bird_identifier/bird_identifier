package birds.api.dto;

public class BirdDto {
    private Integer id;
    private String name;
    private String photo;
    private String flightMode;
    private String flightPattern;
    private String basicColor;
    private String headColor;
    private String headColorCheek;
    private String headColorForehead;
    private String headColorBack;
    private String headColorNeck;
    private String wingsColor;
    private String wingsColorAbove;
    private String wingsColorBelow;
    private String wingsColorPattern;
    private String tailColor;
    private String tailColorAbove;
    private String tailColorBelow;
    private String tailColorPattern;
    private String chestColor;
    private String chestColorPattern;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getFlightMode() {
        return flightMode;
    }

    public void setFlightMode(String flightMode) {
        this.flightMode = flightMode;
    }

    public String getFlightPattern() {
        return flightPattern;
    }

    public void setFlightPattern(String flightPattern) {
        this.flightPattern = flightPattern;
    }

    public String getBasicColor() {
        return basicColor;
    }

    public void setBasicColor(String basicColor) {
        this.basicColor = basicColor;
    }

    public String getHeadColor() {
        return headColor;
    }

    public void setHeadColor(String headColor) {
        this.headColor = headColor;
    }

    public String getHeadColorCheek() {
        return headColorCheek;
    }

    public void setHeadColorCheek(String headColorCheek) {
        this.headColorCheek = headColorCheek;
    }

    public String getHeadColorForehead() {
        return headColorForehead;
    }

    public void setHeadColorForehead(String headColorForehead) {
        this.headColorForehead = headColorForehead;
    }

    public String getHeadColorBack() {
        return headColorBack;
    }

    public void setHeadColorBack(String headColorBack) {
        this.headColorBack = headColorBack;
    }

    public String getHeadColorNeck() {
        return headColorNeck;
    }

    public void setHeadColorNeck(String headColorNeck) {
        this.headColorNeck = headColorNeck;
    }

    public String getWingsColor() {
        return wingsColor;
    }

    public void setWingsColor(String wingsColor) {
        this.wingsColor = wingsColor;
    }

    public String getWingsColorAbove() {
        return wingsColorAbove;
    }

    public void setWingsColorAbove(String wingsColorAbove) {
        this.wingsColorAbove = wingsColorAbove;
    }

    public String getWingsColorBelow() {
        return wingsColorBelow;
    }

    public void setWingsColorBelow(String wingsColorBelow) {
        this.wingsColorBelow = wingsColorBelow;
    }

    public String getWingsColorPattern() {
        return wingsColorPattern;
    }

    public void setWingsColorPattern(String wingsColorPattern) {
        this.wingsColorPattern = wingsColorPattern;
    }

    public String getTailColor() {
        return tailColor;
    }

    public void setTailColor(String tailColor) {
        this.tailColor = tailColor;
    }

    public String getTailColorAbove() {
        return tailColorAbove;
    }

    public void setTailColorAbove(String tailColorAbove) {
        this.tailColorAbove = tailColorAbove;
    }

    public String getTailColorBelow() {
        return tailColorBelow;
    }

    public void setTailColorBelow(String tailColorBelow) {
        this.tailColorBelow = tailColorBelow;
    }

    public String getTailColorPattern() {
        return tailColorPattern;
    }

    public void setTailColorPattern(String tailColorPattern) {
        this.tailColorPattern = tailColorPattern;
    }

    public String getChestColor() {
        return chestColor;
    }

    public void setChestColor(String chestColor) {
        this.chestColor = chestColor;
    }

    public String getChestColorPattern() {
        return chestColorPattern;
    }

    public void setChestColorPattern(String chestColorPattern) {
        this.chestColorPattern = chestColorPattern;
    }
}
