package birds.api.rest;

import birds.api.model.Color;
import birds.api.repository.ColorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/colors")
@CrossOrigin("*")
public class ColorController {

    @Autowired
    private ColorRepository colorRepository;

    @GetMapping
    public List<Color> getColors() {
        return colorRepository.getColors();
    }
//    @PostMapping
//    public void addColor(@RequestBody Color color) {
//        colorRepository.addColor(color);
//    }

//    @DeleteMapping("/{key}")
//    public void deleteColor(@PathVariable("key") String key) {
//        colorRepository.deleteColor(key);
//    }

}