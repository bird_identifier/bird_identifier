package birds.api.rest;

import birds.api.dto.BirdDto;
import birds.api.model.Bird;
import birds.api.repository.BirdRepository;
import birds.api.service.BirdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/birds")
@CrossOrigin("*")
public class BirdController {

    @Autowired
    private BirdRepository birdRepository;

    @Autowired
    private BirdService birdService;

    @GetMapping
    public List<Bird> getAllBirds() {
        return birdService.getBirds();
    }

    @GetMapping("/{id}")
    public Bird getBird(@PathVariable("id") int id) {
        return birdRepository.getBird(id);
    }

    @PostMapping
    public void addBird(@RequestBody Bird bird) {
        birdRepository.addBird(bird);
    }

    @PutMapping
    public void updateBird(@RequestBody Bird bird) { birdRepository.updateBird(bird); }

    @DeleteMapping("/{id}")
    public void deleteBird(@PathVariable("id") int id) {
        birdRepository.deleteBird(id);
    }

}
