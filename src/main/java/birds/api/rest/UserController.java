package birds.api.rest;



import birds.api.dto.GenericResponseDto;
import birds.api.dto.JwtRequestDto;
import birds.api.dto.JwtResponseDto;
import birds.api.dto.UserRegistrationDto;
import birds.api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public GenericResponseDto register(@RequestBody UserRegistrationDto userRegistration) {
        return userService.register(userRegistration);
    }

    @PostMapping("/login")
    public JwtResponseDto authenticate(@RequestBody JwtRequestDto request) throws Exception {
        return userService.authenticate(request);
    }

    @PostMapping("/extend")
    public JwtResponseDto extendSession() {
        return userService.extendSession();
    }
}